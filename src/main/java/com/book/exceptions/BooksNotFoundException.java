package com.book.exceptions;

@SuppressWarnings("serial")
public class BooksNotFoundException extends Exception {

	public BooksNotFoundException(String string) {
		super(string);
	}
	

}
