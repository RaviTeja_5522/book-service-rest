package com.book.responsemodels;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public @Data class BookResponse {
	private Long bookID;

	private String bookName;

	private String author;

	public BookResponse(Long bookID, String bookName, String author) {
		super();
		this.bookID = bookID;
		this.bookName = bookName;
		this.author = author;
	}

	
}
