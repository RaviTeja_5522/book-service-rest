package com.book.controllers;

import java.net.URI;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.book.exceptions.BooksNotFoundException;
import com.book.model.Book;
import com.book.responsemodels.BookResponse;
import com.book.service.BookService;

@RestController
public class BooksController {
	private static final Logger logger = LogManager.getLogger(BooksController.class);

	@Autowired
	private BookService bookservice;
	
	@Value("${service}")
    private String role;

	@GetMapping("/books")
	public ResponseEntity<List<BookResponse>> getAllBooks() throws BooksNotFoundException {
		List<BookResponse> booksResponse = null;
		try {
			booksResponse = bookservice.getAllBookResponses();
		} catch (BooksNotFoundException e) {
			logger.error("Unable to Get Books",e);
			throw new BooksNotFoundException("There are No Books are Available in the Books Repo");
		}
		return ResponseEntity.ok().body(booksResponse);
	}

	@GetMapping("/books/{book_id}")
	public ResponseEntity<BookResponse> getBookbyid(@PathVariable Long book_id) throws BooksNotFoundException {

		BookResponse bookresponse = null;
		try {
			bookresponse = bookservice.getBookResponse(book_id);
		} catch (BooksNotFoundException e) {
			logger.error("Unable to get the book with id:{0}",book_id);
			throw new BooksNotFoundException("There is no Book for given id:" + book_id);

		}
		return ResponseEntity.ok().body(bookresponse);
	}

	@DeleteMapping("/books/{book_id}")
	public HttpStatus deleteBook(@PathVariable Long book_id) throws BooksNotFoundException {
		Book book=null;
		try {
			book=bookservice.deleteBook(book_id);
		} catch (BooksNotFoundException e) {
			logger.error("Unable to Delete the book with id:{0}",book_id,e);
			throw new BooksNotFoundException("There is no Book to delete for given id:" + book_id);
		}
		return HttpStatus.NO_CONTENT;

	}

	@PostMapping("/books")
	public ResponseEntity<BookResponse> addBook(@RequestBody Book book) {

		BookResponse newBook = bookservice.createNewBook(book);
		final URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/books").build().toUri();
		return ResponseEntity.created(uri).body(newBook);
	}

	@PutMapping("/books/{book_id}")
	public ResponseEntity<BookResponse> updateBook(@PathVariable Long book_id, @RequestBody Book newBook)
			throws BooksNotFoundException {
		
		try {
			 bookservice.findBookById(book_id);
		} catch (BooksNotFoundException e) {
			logger.error("Unable to Update the book with id:{0}",book_id,e);
			throw new BooksNotFoundException("There is no Book not founded according to your id:" + book_id);
		}
		return ResponseEntity.ok().body(bookservice.createNewBook(newBook));

	}

}
