package com.book.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public @Data class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long bookID;

	private String bookName;

	private String author;

	public Book(Long bookID, String bookName, String author) {
		super();
		this.bookID = bookID;
		this.bookName = bookName;
		this.author = author;
	}

}
