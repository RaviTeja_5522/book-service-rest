package com.book.service;

import java.util.List;

import com.book.exceptions.BooksNotFoundException;
import com.book.model.Book;
import com.book.responsemodels.BookResponse;

public interface BookService {
	public List<Book> findAllBooks();
	
	public Book findBookById(Long id) throws BooksNotFoundException;
	
	public BookResponse createNewBook(Book book);
	
	public Book deleteBook(Long id) throws BooksNotFoundException;
	
	public List<BookResponse> getAllBookResponses() throws BooksNotFoundException;
	
	public BookResponse getBookResponse(Long id) throws BooksNotFoundException;
	
}
