package com.book.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.book.exceptions.BooksNotFoundException;
import com.book.model.Book;
import com.book.repository.BookRepository;
import com.book.responsemodels.BookResponse;

@Service
public class BookServiceImpl implements BookService {

	private static final Logger logger = LogManager.getLogger(BookServiceImpl.class);
	@Autowired
	private BookRepository bookRepo;

	public List<Book> findAllBooks() {
		return bookRepo.findAll();
	}

	public Book findBookById(Long id) throws BooksNotFoundException {
		return bookRepo.findById(id)
				.orElseThrow(() -> new BooksNotFoundException("Book Not Found for your give id:" + id));
	}

	public BookResponse createNewBook(Book book) {
		book = bookRepo.save(book);
		return new BookResponse(book.getBookID(), book.getBookName(), book.getAuthor());
	}

	public Book deleteBook(Long id) throws BooksNotFoundException {
		Book book = findBookById(id);
		if (book != null) {
			bookRepo.deleteById(id);
		} else {
			logger.error("unable to find book");
			throw new BooksNotFoundException("There is no such book");
		}

		return book;

	}

	public List<BookResponse> getAllBookResponses() throws BooksNotFoundException {
		List<Book> allBooks = findAllBooks();
		List<BookResponse> allBooksResponse = new ArrayList<>();
		if (allBooks.isEmpty()) {
			logger.error("Unable to get books");
			throw new BooksNotFoundException("No books are available");
		} else {

			for (Book book : allBooks) {
				BookResponse bookResponse = new BookResponse(book.getBookID(), book.getBookName(), book.getAuthor());
				allBooksResponse.add(bookResponse);
			}
		}
		return allBooksResponse;
	}

	public BookResponse getBookResponse(Long id) throws BooksNotFoundException {
		Book book = findBookById(id);
		return new BookResponse(book.getBookID(), book.getBookName(), book.getAuthor());

	}

}
