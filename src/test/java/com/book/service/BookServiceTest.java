package com.book.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.book.exceptions.BooksNotFoundException;
import com.book.model.Book;
import com.book.repository.BookRepository;
import com.book.responsemodels.BookResponse;

class BookServiceTest {

	@InjectMocks
	BookServiceImpl bookservice;

	@Mock
	BookRepository bookRepo;

	@BeforeEach
	void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testBookServiceFindAllBooks() {
		List<Book> allBooks = new ArrayList<>();
		long id = 1;
		Book book = new Book(id, "ThreeStates", "shakespere");
		allBooks.add(book);
		when(bookRepo.findAll()).thenReturn(allBooks);
		assertEquals(bookservice.findAllBooks(), allBooks);

	}

	@Test
	void testBookServiceFindBookbyId() throws BooksNotFoundException {

		long id = 1;
		Book book = new Book(id, "ThreeStates", "shakespere");
		Optional<Book> result = Optional.ofNullable(book);

		when(bookRepo.findById(id)).thenReturn(result);
		assertEquals(1, bookservice.findBookById(id).getBookID());

	}

	@Test
	void testBookServiceFindBookbyIdwithNegativeCase() throws BooksNotFoundException {
		long id = 1;
		Book book = new Book(id, "ThreeStates", "shakespere");
		Optional<Book> result = Optional.ofNullable(book);

		when(bookRepo.findById(id)).thenReturn(result);
		assertNotEquals(2, bookservice.findBookById(id).getBookID());

	}

	@Test
	void testCreateBook() {

		long id = 1;
		Book book = new Book(id, "ThreeStates", "shakespere");
		BookResponse bookResponse = new BookResponse(id, "ThreeStates", "shakespere");
        
		when(bookRepo.save(book)).thenReturn(book);
		assertEquals(bookResponse.getBookID(), bookservice.createNewBook(book).getBookID());

	}

	@Test
	void testCreateBookWitNegativeCase() {

		long id = 1;
		Book book = new Book(id, "ThreeStates", "shakespere");
		Book fakebook = new Book((long) 2, "threeSTates", "ronaldo");
		when(bookRepo.save(book)).thenReturn(book);
		assertNotEquals(fakebook, bookservice.createNewBook(book));

	}

	@Test
	void testDelete() throws BooksNotFoundException {
		doNothing().when(bookRepo).deleteById((long) 1);
		assertEquals((long) 1, bookservice.deleteBook((long) 1));

	}

	@Test
	void testDeleteWithNegativeCase() throws BooksNotFoundException {
		doNothing().when(bookRepo).deleteById((long) 1);
		assertNotEquals((long) 2, bookservice.deleteBook((long) 1));

	}

}
