package com.book.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.book.exceptions.BooksNotFoundException;
import com.book.model.Book;
import com.book.responsemodels.BookResponse;
import com.book.service.BookService;

import static org.junit.jupiter.api.Assertions.assertThrows;

class BooksControllerTest {

	@InjectMocks
	BooksController bookController;

	@Mock
	BookService bookservice;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testGetAllBooks() throws BooksNotFoundException {
		List<BookResponse> books = new ArrayList<>();
		books.add(new BookResponse((long) 1, "ChristoperNolen", "DarkNight"));

		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		when(bookservice.getAllBookResponses()).thenReturn(books);
		ResponseEntity<List<BookResponse>> responseEntity = bookController.getAllBooks();
		assertEquals("200 OK", responseEntity.getStatusCode().toString());

	}

	@Test
	void testBookbyId() throws BooksNotFoundException {
		BookResponse book = new BookResponse((long) 1, "ChristoperNolen", "DarkNight");
		when(bookservice.getBookResponse((long) 1)).thenReturn(book);
		ResponseEntity<BookResponse> responseEntity = bookController.getBookbyid((long) 1);
		assertEquals("200 OK", responseEntity.getStatusCode().toString());
		assertEquals(1, responseEntity.getBody().getBookID());

	}

	@Test
	void testBookbyIdwithnegativecase() throws BooksNotFoundException {
		BookResponse book = new BookResponse((long) 1, "ChristoperNolen", "DarkNight");
		when(bookservice.getBookResponse((long) 1)).thenReturn(book);
		ResponseEntity<BookResponse> responseEntity = bookController.getBookbyid((long) 1);
		assertEquals("200 OK", responseEntity.getStatusCode().toString());
		assertNotEquals(2, responseEntity.getBody().getBookID());

	}

	@Test
	void testBookbyIdwithException() throws BooksNotFoundException {
		when(bookservice.getBookResponse((long) 1)).thenThrow(BooksNotFoundException.class);

		assertThrows(BooksNotFoundException.class, () -> bookController.getBookbyid((long) 1));

	}

	@Test
	void testUpdate() throws BooksNotFoundException {
		Book book = new Book((long) 1, "ChristoperNolen", "DarkNight");
		BookResponse updatedBook = new BookResponse((long) 1, "ChristoperNolen", "Inception");
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		when(bookservice.findBookById((long) 1)).thenReturn(book);
		when(bookservice.createNewBook(book)).thenReturn(updatedBook);

		ResponseEntity<BookResponse> responseEntity = bookController.updateBook((long) 1, book);
		assertEquals("200 OK", responseEntity.getStatusCode().toString());
		assertEquals(1, responseEntity.getBody().getBookID());
	}

	@Test
	void testUpdateWithWrongCase() throws BooksNotFoundException {
		Book book = new Book((long) 1, "ChristoperNolen", "DarkNight");
		BookResponse updatedBook = new BookResponse((long) 1, "ChristoperNolen", "Inception");
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		when(bookservice.findBookById((long) 1)).thenReturn(book);
		when(bookservice.createNewBook(book)).thenReturn(updatedBook);

		ResponseEntity<BookResponse> responseEntity = bookController.updateBook((long) 1, book);
		assertEquals("200 OK", responseEntity.getStatusCode().toString());
		assertNotEquals(2, responseEntity.getBody().getBookID());
	}

	@Test
	void testUpdateWithException() throws BooksNotFoundException {
		when(bookservice.findBookById((long) 1)).thenThrow(BooksNotFoundException.class);

		assertThrows(BooksNotFoundException.class, () -> bookController.updateBook((long) 1, null));

	}

	@Test
	void testAddBook() {
		Book book = new Book((long) 1, "ChristoperNolen", "DarkNight");
		BookResponse bookresponse = new BookResponse((long) 1, "ChristoperNolen", "DarkNight");
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		when(bookservice.createNewBook(book)).thenReturn(bookresponse);
		ResponseEntity<BookResponse> responseEntity = bookController.addBook(book);
		assertEquals("201 CREATED", responseEntity.getStatusCode().toString());
		assertEquals(1, responseEntity.getBody().getBookID());

	}

	@Test
	void testAddBookWithNegativeCases() {
		Book book = new Book((long) 1, "ChristoperNolen", "DarkNight");
		BookResponse bookresponse = new BookResponse((long) 1, "ChristoperNolen", "DarkNight");
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		when(bookservice.createNewBook(book)).thenReturn(bookresponse);
		ResponseEntity<BookResponse> responseEntity = bookController.addBook(book);
		assertEquals("201 CREATED", responseEntity.getStatusCode().toString());
		assertNotEquals(2, responseEntity.getBody().getBookID());

	}

}
